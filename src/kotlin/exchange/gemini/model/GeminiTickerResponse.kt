package exchange.gemini.model

data class GeminiTickerResponse(
        val ask: String,
        val bid: String,
        val last: String,
        val volume: Map<String, String>
)