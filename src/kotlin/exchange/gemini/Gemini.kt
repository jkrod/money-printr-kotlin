package exchange.gemini

import com.squareup.moshi.Moshi
import currency.Currency
import exchange.Exchange
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import java.math.BigDecimal

@Suppress("BlockingMethodInNonBlockingContext")
class Gemini(moshi: Moshi) : Exchange {

    private val api: GeminiApi = Retrofit.Builder()
        .baseUrl("https://api.gemini.com")
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()
        .create()

    override val sellFee: BigDecimal = BigDecimal("0.01")
    override val buyFee: BigDecimal = BigDecimal("0.01")
    override val transferFee: BigDecimal = BigDecimal.ZERO

    override val name: String = "Gemini"

    private fun getSymbol(currency1: Currency, currency2: Currency): String {
        return "${currency1.symbol.toLowerCase()}${currency2.symbol.toLowerCase()}"
    }

    override suspend fun getAskPrice(currency1: Currency, currency2: Currency): BigDecimal {
        val symbol = getSymbol(currency1, currency2)
        val ask = api.tickerInfo(symbol)
            .execute()
            .body()
            ?.ask
        return BigDecimal(ask)
    }

    override suspend fun getBidPrice(currency1: Currency, currency2: Currency): BigDecimal {
        val symbol = getSymbol(currency1, currency2)
        val bid = api.tickerInfo(symbol)
            .execute()
            .body()
            ?.bid
        return BigDecimal(bid)
    }
}
