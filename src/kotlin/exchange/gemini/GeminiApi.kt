package exchange.gemini

import exchange.gemini.model.GeminiTickerResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface GeminiApi {

    @GET("v1/pubticker/{symbol}")
    fun tickerInfo(@Path("symbol") symbol: String): Call<GeminiTickerResponse>
}