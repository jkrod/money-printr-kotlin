package exchange.kraken.models

data class KrakenApiResponse<T>(
        val error: List<String>,
        val result: T
)