package exchange.kraken.models

import com.squareup.moshi.Json

data class KrakenTicker(
        @Json(name = "a")
        val askArray: List<String>,
        @Json(name = "b")
        val bidArray: List<String>,
        @Json(name = "c")
        val closedArray: List<String>,
        @Json(name = "v")
        val volumeArray: List<String>,
        @Json(name = "p")
        val volumeWeightedPrice: List<String>,
        @Json(name = "l")
        val lowArray: List<String>,
        @Json(name = "h")
        val highArray: List<String>,
        @Json(name = "o")
        val openingPrice: String
)