package exchange.kraken

import com.squareup.moshi.Moshi
import currency.Currency
import exchange.Exchange
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import java.math.BigDecimal

private fun Currency.toCorrectCurrency(): String {
    return if (this == Currency.BTC) {
        "XBT"
    } else {
        this.symbol
    }
}

@Suppress("BlockingMethodInNonBlockingContext")
class Kraken(moshi: Moshi) : Exchange {

    private val api: KrakenApi = Retrofit.Builder()
        .baseUrl("https://api.kraken.com")
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()
        .create()

    override val sellFee: BigDecimal = BigDecimal("0.0016")
    override val buyFee: BigDecimal = BigDecimal("0.0026")
    // TODO: Really?
    override val transferFee: BigDecimal = BigDecimal.ZERO
    override val name: String = "Kraken"

    private fun getPair(currency1: String, currency2: String): String {
        return "$currency1$currency2"
    }

    override suspend fun getAskPrice(currency1: Currency, currency2: Currency): BigDecimal {
        val correctCurrency1 = currency1.toCorrectCurrency()
        val correctCurrency2 = currency2.toCorrectCurrency()
        val pair = getPair(correctCurrency1, correctCurrency2)
        val askPrice = api.tickerInfo(pair)
            .execute()
            .body()
            ?.result
            ?.get("X${correctCurrency1}Z$correctCurrency2")
            ?.askArray
            ?.get(0)
        return BigDecimal(askPrice)
    }

    override suspend fun getBidPrice(currency1: Currency, currency2: Currency): BigDecimal {
        val correctCurrency1 = currency1.toCorrectCurrency()
        val correctCurrency2 = currency2.toCorrectCurrency()
        val pair = getPair(correctCurrency1, correctCurrency2)
        val bidPrice = api.tickerInfo(pair)
            .execute()
            .body()
            ?.result
            ?.get("X${correctCurrency1}Z$correctCurrency2")
            ?.bidArray
            ?.get(0)
        return BigDecimal(bidPrice)
    }

}
