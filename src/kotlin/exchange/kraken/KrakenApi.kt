package exchange.kraken

import exchange.kraken.models.KrakenApiResponse
import exchange.kraken.models.KrakenTicker
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface KrakenApi {
    //https://www.kraken.com/features/api#get-ticker-info
    @GET("0/public/Ticker")
    fun tickerInfo(@Query("pair") pair: String): Call<KrakenApiResponse<Map<String, KrakenTicker>>>
}