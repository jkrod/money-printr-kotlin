package exchange

import currency.Currency
import java.math.BigDecimal

interface Exchange {
    val sellFee: BigDecimal
    val buyFee: BigDecimal
    val transferFee: BigDecimal
    val name: String
    suspend fun getAskPrice(currency1: Currency, currency2: Currency): BigDecimal
    suspend fun getBidPrice(currency1: Currency, currency2: Currency): BigDecimal
}