package exchange

import currency.Currency
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import java.math.BigDecimal

suspend fun getPossibleProfit(
    amount: BigDecimal,
    buyExchange: Exchange,
    sellExchange: Exchange,
    currency1: Currency = Currency.BTC,
    currency2: Currency = Currency.USD): BigDecimal {
    val sellBid = GlobalScope.async {
        sellExchange.getBidPrice(currency1, currency2)
    }
    val sellFee = sellExchange.sellFee
    val buyAsk = GlobalScope.async {
        buyExchange.getAskPrice(currency1, currency2)
    }
    val buyFee = buyExchange.buyFee
    val actualBuyPrice = buyAsk.await() * (BigDecimal.ONE + buyFee)
    val actualSellPrice = sellBid.await() * (BigDecimal.ONE - sellFee)
    val transferFee = buyExchange.transferFee

    return (amount * (actualSellPrice - actualBuyPrice)) - transferFee
}
