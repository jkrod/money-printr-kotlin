package exchange.coinbase

import com.squareup.moshi.Moshi
import currency.Currency
import exchange.Exchange
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create
import java.math.BigDecimal

@Suppress("BlockingMethodInNonBlockingContext")
class Coinbase(moshi: Moshi) : Exchange {

    private val coinbaseApi: CoinbaseApi = Retrofit.Builder()
        .baseUrl("https://api.pro.coinbase.com")
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .build()
        .create()

    override val sellFee: BigDecimal = BigDecimal.ZERO
    override val buyFee: BigDecimal = BigDecimal("0.003")
    override val transferFee: BigDecimal = BigDecimal.ZERO

    override val name: String = "Coinbase"

    private fun getSymbol(currency1: Currency, currency2: Currency): String {
        return "${currency1.symbol}-${currency2.symbol}"
    }

    override suspend fun getAskPrice(currency1: Currency, currency2: Currency): BigDecimal {
        val symbol = getSymbol(currency1, currency2)
        val response = coinbaseApi.tickerInfo(symbol)
            .execute()
        if (!response.isSuccessful) {
            throw RuntimeException(response.toString())
        }
        val askPrice = response.body()
            ?.ask
        return BigDecimal(askPrice)
    }

    override suspend fun getBidPrice(currency1: Currency, currency2: Currency): BigDecimal {
        val symbol = getSymbol(currency1, currency2)
        val response = coinbaseApi.tickerInfo(symbol)
            .execute()
        if (!response.isSuccessful) {
            throw RuntimeException(response.toString())
        }
        val bidPrice = response.body()
            ?.bid
        return BigDecimal(bidPrice)
    }

}