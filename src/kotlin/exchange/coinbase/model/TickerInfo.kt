package exchange.coinbase.model

data class TickerInfo(
        val trade_id: Long,
        val price: String,
        val size: String,
        val bid: String,
        val ask: String,
        val volume: String,
        val time: String
)