package exchange.coinbase

import exchange.coinbase.model.TickerInfo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface CoinbaseApi {
    @GET("/products/{symbol}/ticker")
    fun tickerInfo(@Path("symbol")symbol: String): Call<TickerInfo>
}