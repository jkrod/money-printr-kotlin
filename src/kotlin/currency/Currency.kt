package currency

enum class Currency(val symbol: String) {
    USD("USD"),
    BTC("BTC"),
    LTC("LTC"),
    ETH("ETH"),
//    ZEC("ZEC")
}