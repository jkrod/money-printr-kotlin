import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import currency.Currency
import exchange.coinbase.Coinbase
import exchange.gemini.Gemini
import exchange.getPossibleProfit
import exchange.kraken.Kraken
import kotlinx.coroutines.runBlocking
import java.lang.Thread.sleep
import java.math.BigDecimal

fun main() {
    val moshi: Moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

    val coinbase = Coinbase(moshi)
    val kraken = Kraken(moshi)
    val gemini = Gemini(moshi)

    val exchanges = listOf(coinbase, kraken, gemini)
    val nonUSDCurrencies = Currency.values()
        .filter { it != Currency.USD }
    while (true) {
        nonUSDCurrencies.forEach { currency ->
            println("Testing currency $currency")
            exchanges.forEach { ex1 ->
                exchanges.forEach { ex2 ->
                    if (ex1 != ex2) {
                        val potentialProfit = runBlocking {
                            getPossibleProfit(
                                currency1 = currency,
                                currency2 = Currency.USD,
                                amount = BigDecimal.ONE,
                                buyExchange = ex1,
                                sellExchange = ex2)
                        }
                        println("Buy ex: ${ex1.name} sell ex: ${ex2.name} $$$$ $potentialProfit")
                    }
                }
            }
        }
        println("+_+_+_+_+_+_+_+_+_+_+_+_")
        sleep(5000)
    }
}
