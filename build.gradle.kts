import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "gg.jan.moneyprintr"
version = "1.0-SNAPSHOT"

val ktVersion: String = "1.3.21"
plugins {
    kotlin("jvm") version "1.3.21"
}

sourceSets["main"].java.srcDir("src/kotlin")

dependencies {
    val retrofitVersion = "2.5.0"
    implementation(kotlin("stdlib-jdk8", ktVersion))
    implementation(kotlin("kotlin-reflect", ktVersion))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.1.1")
    implementation("com.squareup.retrofit2:retrofit:$retrofitVersion")
    implementation("com.squareup.retrofit2:converter-moshi:$retrofitVersion")
    implementation("com.squareup.moshi:moshi-kotlin:1.8.0")
}

repositories {
    mavenCentral()
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}

val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
